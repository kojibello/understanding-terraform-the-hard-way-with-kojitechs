account_ids = {
     prod = "735972722491"
}

vpc_cidr            = "100.0.0.0/16"
public_subnetcidr   = ["100.0.0.0/24", "100.0.2.0/24", "100.0.4.0/24"]
private_subnetcidr  = ["100.0.1.0/24", "100.0.3.0/24", "100.0.5.0/24"]
database_subnetcidr = ["100.0.51.0/24", "100.0.53.0/24", "100.0.55.0/24"]
instance_type       = "t3.large"
instance_class      = "db.t2.micro"
username            = "kojitechs"

dns_name                  = "kojitechskart.net"
subject_alternative_names = ["*.kojitechskart.net"]
