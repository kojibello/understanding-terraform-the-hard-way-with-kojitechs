# Terraform block
terraform {
    required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  profile = "default" # 
}


locals {
  vpc_id  = aws_vpc.kojitechs-vpc.id
  # azs = data.aws_availability_zones.azs.names
}


# # resource_name    local_resource name
resource "aws_vpc" "kojitechs-vpc" {
  cidr_block = var.vpc_cidr
}

resource "aws_subnet" "private" {
    count = length(var.private_subnets_cidr) 

  vpc_id     =  local.vpc_id 
  cidr_block =  var.private_subnets_cidr[count.index] 
  availability_zone = slice(data.aws_availability_zones.azs.names, 0,2)[count.index] # element(slice(local.azs, 0,2), count.index) # 

  tags = {
    Name = "private_subnet"
  }
}


# output "private_subnet_without_count" {
#     value = aws_subnet.private.id # didn't use count or for_each 
# }

# legacy splat operator 
output "private_subnet_with_count" {
    value = aws_subnet.private.*.id # didn't use count or for_each 
}

## latest splat operator 
output "private_subnet_with_count_latest_splat" {
    value = aws_subnet.private[*].id # didn't use count or for_each 
}

# output just a single subnet id
output "private_subnet_single_subnet_id" {
    value = aws_subnet.private[0].id # didn't use count or for_each 
}
#     0,               1,              2,            3 ,         4
# ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24", "10.0.9.0/24"]


## 
# resource "aws_subnet" "private2" {
#   vpc_id     =  local.vpc_id 
#   cidr_block = "10.0.3.0/24" # var.variable 
#   availability_zone =  data.aws_availability_zones.available.names[1] 
# }

# ## 
# resource "aws_subnet" "private3" {
#   vpc_id     = local.vpc_id 
#   cidr_block = "10.0.5.0/24" # var.variable 
#   availability_zone =  data.aws_availability_zones.available.names[0]
# }

# ## 
# resource "aws_subnet" "private4" {
#   vpc_id     = local.vpc_id 
#   cidr_block = "10.0.7.0/24" # var.variable 
#   availability_zone =  data.aws_availability_zones.available.names[1] # "us-west-1f"
# }
# ## 
# resource "aws_subnet" "private4" {
#   vpc_id     = local.vpc_id 
#   cidr_block = "10.0.7.0/24" # var.variable 
#   availability_zone =  data.aws_availability_zones.available.names[1] # "us-west-1f"
# }

# RESOURCE BLOCK  # private_ip = 
resource "aws_instance"  "ec2_instance" {
  ami           = data.aws_ami.ami.id # 
  instance_type = "t2.micro"

  tags = {
    Name = "ec2_instance"
  } 
}

## How to use data source to pull down an ami!

## Functions 
### slice() # [1,2,3,4] => specific [1,2]
## length(var.private_subnets_cidr) # 
## count.index
# 
# 
# ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24", "10.0.9.0/24", "929283848"] 
# ["us-east-1a",  "us-east-1b"]
#  element(slice(local.azs, 0,2), count.index)