
output "vpc_id" {
    description = "value for vpc_id"
    value = local.vpc_id
}

output "public_subnets" {
    description = "value for public subnet ids"
    value = module.vpc.public_subnets
}

output "private_subnets" {
    description = "value for private subnet ids"
    value = module.vpc.priavate_subnets
}

output "database_subnets" {
    description = "value for database subnet ids"
    value = module.vpc.database_subnets
}