
###############################################################################
# CREATING APP1 SECUIRTY  GROUP
###############################################################################

resource "aws_security_group" "app_static_sg" {
  name        = "${var.component}-app-static-sg"
  description = "Allow alb on port 80"
  vpc_id      = local.vpc_id

  tags = {
    Name = "${var.component}-app-static-sg"
  }
}

resource "aws_security_group_rule" "app_ingress_access_on_http" {

  security_group_id        = aws_security_group.app_static_sg.id
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "egress_access_from_for_static" {
  security_group_id = aws_security_group.app_static_sg.id
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = ["0.0.0.0/0"]
}
